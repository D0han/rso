<?php

$db_read_host   = 'localhost';
$db_read_user   = 'asl24';
$db_read_pass   = 'qwerty';

$db_write_host  = '192.168.1.131';
$db_write_user  = 'asl24';
$db_write_pass  = 'qwerty';

$db_name        = 'baza1';

$rabbitmq_host  = '192.168.1.129';

$memcache_host  = 'localhost';
$memcache_port  = 11211;
$memcache_time  = 30;   // 30 sekund cache

// -----------------------------------------------------------
// TODO: OGARNAC SESJE W REDIS
//ini_set('session.save_handler', 'redis');
//ini_set('session.cookie_domain', '.localhost.localhost');
//ini_set('session.save_path', 'localhost');
//session_set_cookie_params(0, '/', '.localhost.localhost');
//session_name('localhostest');

$db_write_error = 0;

$db_read = new mysqli($db_read_host, $db_read_user, $db_read_pass, $db_name);
if (mysqli_connect_errno()) {
    printf('<center><h1>Przerwa techniczna (db_read)</h1></center><br /><a href="javascript:history.go(-1)">[Wstecz]</a>');
    exit();
}
$db_write = new mysqli($db_write_host, $db_write_user, $db_write_pass, $db_name);
if (mysqli_connect_errno()) {
    $db_write_error = 1;
}

?>