<?php
include_once('config.php');
#session_start();
include_once('SessionHandler.php');


if ($_SESSION['admin'] != True) {
    die("poszedl won!");
} else if ($db_write_error) {
    die('Przerwa techniczna');
}

echo 'Jestes zalogowany jako '.$_SESSION['imie'].' '.$_SESSION['nazwisko'].' ('.$_SESSION['login'].'), tutaj mozesz moderowac nowe wpisy na tablicy<br />';

$cnn = new AMQPConnection();
$cnn->setHost($rabbitmq_host);
$cnn->connect();

// Create a channel
$ch = new AMQPChannel($cnn);

// Declare a new exchange
$ex = new AMQPExchange($ch);
$ex->setName('exchange1');
$ex->setType("fanout");
$ex->declare();

// Create a new queue
$q = new AMQPQueue($ch);
$q->setName('queue1');
$q->declare();

// Bind it on the exchange to routing.key
$q->bind('exchange1', 'routing.key');

echo 'Oczekujace wpisy:<br />';
#echo '<form name="moderacja" action="moderacja.php" method="post">';
echo '<table border="1">';
echo '<tr><td>user_id</td><td>imie</td><td>nazwisko</td><td>login</td><td>tekst</td><td>akceptuj</td><td>wywal</td></tr>';
//get the messages
while ($message = $q->get()) {
    $wpis = unserialize($message->getBody());
    $tag = $message->getDeliveryTag();
    
    if (isset($_GET['add'])) {
        if ($_GET['add'] == $tag) {
            $query = "insert into tablica values (NULL, '".$wpis['user_id']."', '".$wpis['tekst']."')";
            $result = $db_write->query($query) or die($db_write->error.__LINE__);
            $q->ack($tag);
            
            $memcache = new Memcache;
            $memcache->connect($memcache_host, $memcache_port) or die ("Could not connect (memcache)");
            $key = md5('tablica');
            $memcache->delete($key);
            
            continue;
        }
    } else if (isset($_GET['del'])) {
        if ($_GET['del'] == $tag) {
            $q->ack($tag);
            continue;
        }
    }
    
    $link_add = '<a href="moderacja.php?add='. $tag .'">akceptuj</a>';
    $link_del = '<a href="moderacja.php?del='. $tag .'">wywal</a>';
    echo '<tr><td>'.$wpis['user_id'].'</td><td>'.$wpis['imie'].'</td><td>'.$wpis['nazwisko'].'</td><td>'.$wpis['login'].'</td><td>'.$wpis['tekst'].'</td><td>'.$link_add.'</td><td>'.$link_del.'</td></tr>';
    
}
echo '</table>';

mysqli_close($db_read);
?>
<br /><br /><a href="index.php">index</a>