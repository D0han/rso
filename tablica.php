<?php
include_once('config.php');
#session_start();
include_once('SessionHandler.php');

if (isset($_SESSION['login'])) {
    echo 'Jestes zalogowany jako '.$_SESSION['imie'].' '.$_SESSION['nazwisko'].' ('.$_SESSION['login'].')<br />';
    echo '<br />Wyslij nowy wpis:<br /><form name="wpis" action="tablica.php" method="post">
        <textarea cols="40" rows="5" name="tekst"></textarea><br />
        <input type="submit" value="Dodaj wpis do moderacji">
        </form><br /><br />';
        
    if (isset($_POST['tekst'])) {
        $tekst = $db_read->real_escape_string($_POST['tekst']);
        // RABBITMQ
        // Create a connection
        $cnn = new AMQPConnection();
        $cnn->setHost($rabbitmq_host);
        $cnn->connect();

        // Create a channel
        $ch = new AMQPChannel($cnn);

        // Declare a new exchange
        $ex = new AMQPExchange($ch);
        $ex->setName('exchange1');
        $ex->setType("fanout");
        $ex->declare();
        
        $wpis = array("tekst" => $tekst,
                      "login" => $_SESSION['login'],
                      "imie" => $_SESSION['imie'],
                      "nazwisko" => $_SESSION['nazwisko'],
                      "user_id" => $_SESSION['user_id']);
        
        // Publish a message to the exchange with a routing key
        $wyn = $ex->publish(serialize($wpis), 'routing.key');
        
        if ($wyn) {
            echo '<center><h2>Twoj wpis zostal wyslany do moderacji</h2></center><br /><br />';
        } else {
            echo '<center><h2>Twoj wpis <strong>NIE</strong> zostal wyslany</h2></center><br /><br />';
        }
            
    }
} 

$memcache = new Memcache;
$memcache->connect($memcache_host, $memcache_port) or die ("Could not connect (memcache)");

$key = md5('tablica');
$cache_result = '';
$cache_result = $memcache->get($key);

if($cache_result) {
    // Second User Request
    $tablica=$cache_result;
} else {
    // First User Request 
    $query = "select users.imie, users.nazwisko, tablica.tekst from tablica join users on tablica.user_id = users.user_id order by tablica.id desc;";
    $result = $db_read->query($query) or die($db_read->error.__LINE__);
    
    $tablica = '';
    //var_dump($result);
    if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            //var_dump($row);
            $tablica .= $row['imie'].' '.$row['nazwisko'].': '. $row['tekst'] . '<br /><br />';
        }
    }
    else {
        $tablica .= 'Tablica jest pusta';	
    }
    
    $tablica .= '<br /><a href="tablica.php">odswiez tablice</a><br /><br />';
}


// memcache
if (! $cache_result) {
    $memcache->set($key, $tablica, MEMCACHE_COMPRESSED, $memcache_time);
}
echo $tablica;

mysqli_close($db_read);
?>
<br /><br /><a href="index.php">index</a>